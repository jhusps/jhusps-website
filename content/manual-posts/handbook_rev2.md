Title: Handbook, Revision 2
Date: 2015-07-27 15:05
Category: Resources
Author: Ben Rosser
Slug: handbook-rev2

The SPS board has worked on updating the previous version of the handbook
and is now proud to present revision 2!

[Click here]({filename}/html/handbook/handbook_rev2.pdf) for a PDF copy.

As before:

If you've got suggestions, comments, complaints, objections, or any other sort
of feedback, please feel free to get in touch with us (see the links on the
right, or on [this page]({filename}/pages/contact.md)). As we update the handbook,
we'll announce it on here.
