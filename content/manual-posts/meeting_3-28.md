Title: Meeting 3/28
Date: 2014-03-27 11:00
Category: Announce
Author: Vittorio Loprinzo
Slug: announce-2014-03-28

I hope everyone enjoyed their Spring Break!

Thanks to our webmaster, Ben, I can now post announcements here, so you should begin to see them fairly regularly. Check back often!

Our next meeting will be **Friday 3/28, at 4:00pm in Bloomberg 462**. As you may know,
 a team of physicists last week announced that they've detected evidence of gravitational
 waves. This is kind of a big deal, so we've got **Professor Marc Kamionkowski** coming to
 our meeting to give a talk on the subject. He's been all over the news lately, since this 
 experimental evidence supports many of his theories. This talk should be right on the cutting 
 edge of physics, so please attend!

Also, just an aside: SPS went on a department-funded trip to California last week to visit
some national labs. We went to SLAC, LBL, and many interesting sites around the San Francisco
area. If you attended the trip, please gather your photos and receipts and bring them to the
meeting!

-Vittorio L.
