Title: Internship and research opportunities
Date: 2014-09-07 20:05
Category: Resources
Author: Ben Rosser
Slug: resources

Adding to our list of resources (the "Handbook" category has been renamed
to "Resources"), here's a collection of things research opportunities and
internships. More will almost certainly be added here in the future.

First, [this page](https://www.nsf.gov/crssprgm/reu/reu_search.jsp), which is 
also linked in our sidebar, is from the NSF and is a large list of possible
internships.

Other resources include:

* [Nucleus (from National SPS)](http://www.compadre.org/student/research/)
* [SULI (National Laboratories)](http://science.energy.gov/wdts/suli/)
