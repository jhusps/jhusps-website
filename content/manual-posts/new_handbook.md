Title: The Handbook!
Date: 2014-09-02 21:16
Category: Resources
Author: Ben Rosser
Slug: new-handbook

We've finally gotten far enough along in the Physics Major Handbook that
we feel comfortable talking about it more here.

As I mentioned before, this is a student-created guide intended to discuss
the physics major from a student perspective, providing insight we've accumulated
over the years for freshmen and incoming students. The goal is to prevent each
class from having to learn the same lessons over and over again.

Most of the credit goes to Vittorio Loprinzo for getting this done, but the
rest of the current board provided feedback.

[Click here]({filename}/html/handbook/handbook_rev1.pdf) for a PDF copy.

If you've got suggestions, comments, complaints, objections, or any other sort
of feedback, please feel free to get in touch with us (see the links on the
right, or on [this page]({filename}/pages/contact.md)). As we update the handbook,
we'll announce it on here.
