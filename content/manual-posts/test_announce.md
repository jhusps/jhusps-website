Title: Welcome to the new site!
Date: 2014-03-03 09:17
Category: Announce
Author: Ben Rosser
Slug: announce-test

Welcome to the new SPS website!

This is all currently a work in progress, as you might be able to tell, and is
liable to change drastically as people yell at me and tell me to fix things.

All of the content from the old site has been ported over, however, so I think
we're good to go!

Also, for anyone actually reading this, let me plug our new IRC channel, #jhusps
(see [this page]({filename}/pages/contact.md) for details). Come join us (or,
rather, me, because I'm the only one there at the moment).

Stuff we will ideally have eventually: 

* A calendar system under the Meetings section that actually works
* A page on the history of our chapter
* Announcements (for meetings, primarily) on the front page
* More information about the office and what we have there
* A member list of some sort or another

Ben Rosser

