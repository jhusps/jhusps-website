Title: Pictures from the California trip!
Date: 2014-07-13 15:55
Category: Galleries
Author: Ben Rosser
Slug: galleries
gallery:california

So, I finally did this. Galleries are now a thing; we can add image galleries
to the site and they will be rendered.

The software wanted to convert everything into a 200x200 thumbnail, which is
why you see what you see below. (Click on this post to see them). Clicking
on a picture will bring up the larger version.

This may seem silly to you, but believe me, I tried disabling the thumbnailer
and it was *much* sillier. Our gallery technology may improve in the future,
but hey, at least we have pictures on the site now!

(Old galleries from the old site will probably get imported soon).
