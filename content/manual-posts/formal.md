Title: This week's meeting
Date: 2014-10-19 14:53
Category: Announce
Author: Lauren Aldoroty
Slug: announce-2014-10-24

The JHU Society of Physics Students and JHU JOSH will host a formal dance! There 
will be music and food in the Bloomberg ground floor telescope rotunda, and SPS
members will take you up to the rooftop observatory for stargazing. The event
is free and open to everyone, and donations will be accepted at the door.

The formal will be on 10/24 between 7:30 PM and 12 AM. See you there!
