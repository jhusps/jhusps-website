Title: Spring 2016 Update
Date: 2016-03-06 14:21
Category: Announce
Author: Vittorio Loprinzo
Slug: spring-2016-update

Another long overdue update! 

Our SPS has just been designated as a Distinguished Chapter by the national SPS governance for 2014-15. That's two awards in a row; let's keep the streak going!

SPS has recently hosted numerous events, including:

* A talk from Prof Bob Leheny on anisotropic fluids
* Our third annual Murder Mystery
* A talk from Prof David Neufeld entitled "The Molecular Universe"
* An undergrad research Q&A with three members of the PHA faculty
* Our third annual Super Bowl celebration
* A physics-themed Family Feud
* And another biennial LAN Party!

Coming up, we have several more talks and our second annual Stargazing Formal. We're also planning a lesson for high schoolers during JHU Splash, and building a trebuchet for the annual physics fair!

-Vittorio