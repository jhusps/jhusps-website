Title: APS Professional Guidebook
Date: 2015-10-30 15:05
Category: Resources
Author: Ben Rosser
Slug: aps-professional-guidebook

Vittorio asked me to post this on the site; this is the American Physical
Society's "Professional Guidebook", providing physics career advice!
Potentially very useful for seniors.

[Here is a link](http://www.aps.org/careers/guidance/development/index.cfm?utm_source=APS+Physics+Main+Group&utm_campaign=007cd2cc5c-Vector_September_2015&utm_medium=email&utm_term=0_825303224b-007cd2cc5c-107711905&mc_cid=007cd2cc5c&mc_eid=525bfbbab6).
