Title: Update
Date: 2015-10-30 15:48
Category: Announce
Author: Vittorio Loprinzo
Slug: update

Hey SPS! It's been a while since we updated the front page of our website with upcoming meetings; lately, we've been communicating about meetings via our email list and posting resources to our website.

Here's a list of meetings and events we held so far this semester:

* The 1st Annual Physics Olympics (congrats to Joe, Alec, and Kyle, our champions!)

* A lecture from Prof Oleg Tchernyshyov on magnetic phenomena

* We helped PAGS with the White House Astronomy Night

* A talk from and a visit to the lab of Prof Stephan McCandliss

* A series of five-minute presentations from members on their current research projects

* A game day, including traditions like the Largest Integer Game and the SPS Scavenger Hunt

* A start-of-the-year BBQ with the PHA faculty

This week, we'll be honoring another old SPS tradition by screening the C-list sci-fi classic *Primer* for Halloween. Get ready to get confused about time travel.

Check back here often for updates on our activities this semester, and don't forget to subscribe to our [mailing list](https://lists.acm.jhu.edu/mailman/listinfo/sps)!

-Vittorio
