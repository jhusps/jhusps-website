Title: Applying to Graduate School, from SPS in 2004
Date: 2014-07-22 11:53
Category: Resources
Author: Ben Rosser
Slug: old-handbook

One initiative we're currently working on over the summer is developing a
student-created handbook for the physics major. Vittorio has taken the
initiative here, but we're building on the work begun by the Class of 2012.
Expect that to be posted relatively soon.

Why am I talking about it now, if it is not finished?

The old SPS websites contain a lot of valuable resources that nearly fell off
the internet. We'll be reposting them, along with our own guides, under the
"Handbook" category; click the Handbook button on the sidebar to see them all
in one convenient place. (Once we have more material available, anyway).

Today's topic: Applying to Graduate School, courtesy of the 2004-2005 chapter.

[Click here to read it!]({filename}/html/grad2004/gradint.html)
