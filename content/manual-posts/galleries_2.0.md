Title: Pictures from the California trip, again!
Date: 2015-03-04 16:55
Category: Galleries
Author: Ben Rosser
Slug: galleries_2.0

I have upgraded our gallery technology to something significantly better than
what we had before, both admin-side and user-side. Our photos now all live
[here](https://www.acm.jhu.edu/~sps/PhotoFloat/web/), living in the storage
space provided for us by the Johns Hopkins [ACM](https://www.acm.jhu.edu/).

New galleries (including the photos from the Brookhaven trip, which should
be up in a day or two) will be posted there and linked in all the right
places here.

In the mean time, I have updated all of our references to the California gallery
that was posted on the site previously.
