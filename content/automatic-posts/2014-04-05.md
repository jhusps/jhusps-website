Title: New SPS Board 2014-15
Date: 2014-04-05 10:04
Category: Announce
Author: Vittorio Loprinzo
Slug: announce-2014-04-05

At our meeting on Friday 4/4 (which was not announced here because I had tech trouble), SPS elected its new officers for the upcoming academic year. They are:

* President - Vittorio Loprinzo
* VP - Joseph Cleary
* Outreach - Carolyn Ortega
* Technology - Ben Rosser
* Secretary - Lauren Aldoroty
* Treasurer - Julia Chavarry

I'm looking forward to a second term as President, and to taking the club to new places with the new board!