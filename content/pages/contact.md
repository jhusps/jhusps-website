Title: Contact

# SPS Board

The 2016-2017 SPS board members are:

* President: [Tarini Konchady](mailto:tkoncha1@jhu.edu)
* Vice President: [Alec Farid](mailto:afarid1@jhu.edu)
* Activities Officer: [Theo Cooper](mailto:tcoope21@jhu.edu)
* Technology Officer: [Carolyn Ortega](mailto:cortega7@jhu.edu)
* Secretary: [Kyle Sullivan](mailto:ksulli44@jhu.edu)
* Treasurer: [Anthony Flores](mailto:aflore22@jhu.edu)
* Women in Physics Chair: [Anna Carter](mailto:acarte33@jhu.edu)

You can contact any of us through email, though please do try and send any
inquiries to the right person.  
  
# Chat

We have an IRC channel! Join us at [#jhusps]({filename}/html/chat.html) on
chat.freenode.net, either by clicking the link and using our built-in webchat
or by connecting manually.

# Mailing Address

### Our Address:
Society of Physics Students  
Department of Physics and Astronomy  
The Johns Hopkins University  
Baltimore, MD 21218-2686 USA 

### National Office:
National Office  
Society Of Physics Students  
One Physics Ellipse  
College Park, MD 20740  
Telephone: (301) 209-3007  
Fax: (301) 209-0839  
E-Mail: <sps@aip.org>  
Website: www.spsnational.org  

# Department Contacts

### Faculty Advisor
Prof. Petar Maksimovic  
Bloomberg 417  
(410) 516-3819  
<petar@jhu.edu>  

### Department Chair
Prof. Timothy Heckman    
Bloomberg 523    
(410) 516-7369      
<heckman@pha.jhu.edu>    

### Director of Undergraduate Studies
Prof. Robert Leheny     
Bloomberg 353      
(410) 516-6442      
<leheny@jhu.edu>     
