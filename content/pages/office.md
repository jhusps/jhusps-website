Title: Office

Our office is right outside the [PUC Lab](http://web1.johnshopkins.edu/puc/), in
**Bloomberg Room 476**. You can get in with the standard Bloomberg key. If you
don't have a key, if you are a physics major you can request one from the front
desk in Room 366 for $5.

We have recently cleaned a lot of things out! So, it is currently a work in 
progress (much like this website), however we do at this moment have some books
and a fridge containing drinks.

# Books

We are amassing a small library of physics textbooks and other interesting
books for our members' use.

If you have a book you'd like to donate, please
[get in touch]({filename}/pages/contact.md). We would almost certainly welcome
it!

## Contributors

People who have given us stuff (books or otherwise):

* Prof Bruce Barnett: many books

If you gave us something but are not listed here, please let us know.

## Books we have:

* A printed copy of the Waves textbook, for 171.201
* Many more that need to be put on here
* A lot of [old graduate programs](http://www.pha.jhu.edu/groups/sps/historical/Old%20--%202003-2004//grad/gradlist.html)
(that may be significantly out of date).
