Title: About

# Welcome to SPS

The Society of Physics Students (SPS) has more than 6,000 members in nearly 700
colleges and universities. An active SPS chapter provides the professional 
development opportunities that course work cannot teach, including development
of communication and professional networking skills, undergraduate research and
science literacy outreach to the community.

The SPS chapter can also be a physics department's most effective recruitment
and retention tool. An active chapter helps to transform students from being
mere class attendees to being contributing members of a professional community.

And it certainly does not require its members to be physics students! Anyone
interested in physics or its applications can be a member of both the national
organization and the local chapter.

The Society of Physics Students at Johns Hopkins is the 3,371st chapter of SPS.
We believe SPS has great effects on communications between undergraduate
physics students at Hopkins. Not only is SPS responsible for the information
exchange and link between the faculty and the students, but it also serves as
a social environment for undergraduate physics majors.

Being a member here at JHU means coming to the meetings, being on the JHU SPS
mailing list, being eligible for the annual trip and having access to the SPS
office.  To join the national SPS, you must send $20 and an information form to
the national headquarters. For more information, go 
[here](http://www.spsnational.org/about/benefits.htm).

If you enjoy hearing about current research in physics, helping others to
understand physics and having fun, then you should consider being a member of
the Society of Physics Students at JHU. We hope to see you at our meetings! 

# Join SPS

If you'd like to become a member of SPS at Hopkins, come to one of our
meetings (there are sign-up sheets at every meeting).

Anyone is welcome! If you have an interest in physics, we want you at our
meetings, no matter your major.

And plus, we have free pizza.

# Meetings at a glance:

All [meetings]({filename}/pages/meetings.md) are Fridays at 4:00 PM in
Bloomberg room 462 and usually last an hour (unless announced otherwise).
Meetings will usually alternate between a guest lecturer and a more social
event.

# Constitution and By-Laws:

Effective as of summer/fall 2014, ratified by SPS in Spring 2014.

SPS Bylaws:

## I. Membership

### A. Eligibility

1. Membership is open to all, regardless of major, student status, or any characteristic defined in Johns Hopkins student group general policy.
2. Individuals may join at any meeting or event by signing in.
3. Membership lists are maintained by the Secretary. A point system is used to determine membership status.

### B. Membership Status

1. Individuals become members after listing their information on a sign-in sheet at any meeting or event.
2. Meetings, events, surveys, outreach, class standing, officership, and other club activities are assigned point values at the discretion of the Secretary.
3. After amassing fifteen points, members are added to the roster on the club website.
4. After amassing twenty-five points, members may request a key to the club office.
    * This key may be revoked at any time at the discretion of the officers due to: a semester of inactivity; unpaid fees from office food purchases; other offending actions.
5. All members are required to assist with at least one outreach activity per academic year to maintain club office access. Assistance can range from volunteering at the event to advertisement to planning logistics.
6. All members are encouraged to assist with introductory physics tutoring at the university when they are able and called upon to do so; this may include face-to-face tutoring or time in an electronic help room.

## II. Motions & Voting

1. Members may motion to hold a vote about various issues at any meeting or by communicating with the board in advance of a meeting. Any motion seconded by another member passes and a vote is held at the same meeting or at the discretion of the board.
    *. Voting privileges can be granted to nonmembers with approval of half of the board.
2. Votes on issues are passed according to the ratios required by their respective areas of the Constitution.
    *. A quorum is considered to be fifty percent of all members listed on the website.

## III. Officers

### A. Elections

1. Board elections are held near the end of every academic year. All board positions are put to a vote, and current board members must be reelected to serve another term.
2. Positions are elected from the top of the chain of command downwards. Members who are not elected to one position are permitted to run for others.
3. Any or all board positions may be put to election at any time after a two-thirds-approved motion. 
4. All officers must be undergraduates at Johns Hopkins, with the exception of the Technology officer if no qualified undergraduate can be found.
5. Officer nominations are made as motions. Members may motion to nominate themselves.
6. Nominated members may not be present for voting.
7. Any number of nominations for any position may occur. The nominated member receiving a plurality of votes takes the position.
    * Ties are broken by a second vote including only the tied nominees.
    * Further ties are broken by a board vote, with the President (or Vice President in a presidential vote) breaking any final ties.
8. All elected board members must receive at least one vote.
9. No more than four of six officers may come from a single graduating class, unless no other nominees can be found.
10. No individual may serve more than two consecutive terms as President.
11. Members chosen to take officership during annual elections are treated as “officer-elects” until the end of that academic year. Officer-elects assume the responsibilities of their respective positions, but are assisted and advised by their predecessors until the end of the year.

### B. Responsibilities

1. All officers are expected to shoulder a share of the chapter’s workload. This includes, and is not limited to, taking charge of and planning activities, assisting the department with campus events and recruitment as needed, and assisting other officers with their responsibilities when necessary.
2. All officers should attend the majority of SPS meetings.
3. All officers are expected to attend officers’ meetings as necessary. Any officer may call an officer meeting.
4. Officers meetings should be held at least once a month if possible.

[Chain of command extends from top down]

1. President: Plans and runs all meetings and events, including speakers, social activities, and department feedback session. Delegates responsibilities and takes on those others cannot carry out. Assists department with publicity events with Outreach. Maintains email list and conveys info to club. Maintains club’s status as a Hopkins student organization. 
2. Vice President: Responsible for running meetings in the President’s absence. Assists the President in club organization and delegation of responsibilities. Responsible for planning the Spring Break trip, including itinerary, travel information and (with the assistance of the Treasurer), budget.
3. Communications Officer: Responsible for cultivating and maintaining relationships with faculty contacts and various other student organizations as necessary.
4. Outreach Officer: Works with Physics department faculty to organize events that put SPS in contact with the Baltimore community. Handles affairs with prospective students with President. Photographs events. 
5. Technology Officer: Responsible for maintenance of the Chapter website and SPS office computer if it exists. Handles all technical aspects of events, including setting up servers, obtaining AV equipment, ensuring multimedia equipment is working in advance, etc. Acts as liaison to the department about technology-related issues.
6. Secretary: Responsible for keeping track of membership point list, as well as creating and collecting sign-in sheets at each meeting. Responsible for maintenance of club bulletin boards and SPS library. Carries out correspondence with President. Determines who qualifies for SPS privileges. Files annual chapter report in May.
7. Treasurer: Maintains an actively updated club budget sheet. Responsible for all meeting and event purchases and reimbursements. Manages SPS office purchase list.

## IV. Meetings

1. Meetings are organized and called by the President.
2. Meeting times are put to a vote at the end of each semester.
3. Meeting times can be put to a vote at any time by a majority-approved motion. The opportunity to vote for meeting times is offered to all club members, including those who cannot attend meetings.  

## V. Sigma Pi Sigma

1. Membership requirements for Sigma Pi Sigma are determined by the department.
2. Inductions into Sigma Pi Sigma are held at the end of each academic year.
3. The membership roster for Sigma Pi Sigma is maintained by the Secretary.

## VI. Miscellaneous Policies

1. Members are encouraged but not required to join the National SPS organization.
2. The only fees charged to members are for optional purchases, such as food from the club office.
    * If a member is unable to pay a fee necessary for full attendance at an event, the member may work with the officers to be assisted with payment by the club.
    * If a member does not pay fees amassed by the deadlines set by the Treasurer at the end of each semester, access to the club office will be revoked. 
3. At the end of each academic year, the club meets with department faculty to discuss potential changes to department policy and give feedback on the current state of the undergraduate physics experience.
4. Members are invited to events, including the annual trip, based on the point system maintained by the Secretary. Members with the most points are given preference, unless a particular member must attend for a reason critical to the event’s operation.
5. The club abides by all Johns Hopkins University policies, including those related to harassment. Any member may approach the President with any issue.

## VII. Amendments

1. Constitutional amendments may be proposed by any member at any meeting by way of a motion.
2. Amendments must be passed by a majority in the club body and half of the board’s approval.

