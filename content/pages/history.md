Title: History

Some chapter history (this page is a work in progress).

## Previous Boards

### 2014-2015

* President: [Vittorio Loprinzo](mailto:vloprin1@jhu.edu)
* Vice President: [Joseph Cleary](mailto:jcleary3@jhu.edu)
* Outreach Officer: [Carolyn Ortega](mailto:cortega7@jhu.edu)
* Technology Officer: [Ben Rosser](mailto:bjr@acm.jhu.edu)
* Secretary: [Lauren Aldoroty](mailto:laldoro1@jhu.edu)
* Treasurer: [Julia Chavarry](mailto:jchavar3@jhu.edu)
* Women in Physics Chair: [Grace McClintock](mailto:gmcclin1@jhu.edu)
