Title: Members

&#931; indicates membership in Sigma Pi Sigma, our honor society. More info
on Sigma Pi Sigma will be added to the website soon!

If you would like to add a link to a personal website or an email address,
please [contact the webmaster]({filename}/pages/contact.md).

# Current Members

### Seniors (Class of 2016)
* Vittorio Loprinzo &#931;
* Ben Rosser &#931;
* Joseph Cleary &#931;
* Alex de la Vega
* Bridget Ratcliffe
* Streit Cunningham
* Kirk Trombley
* Zhuo (Ben) Zhang &#931;
* Philip Piantone
* Dario Verta
* Ivan Rasmussen
* Neil Chapel
* Brent Yelle
* Ben Reis
* Logan Dorman
* Grace McClintock
* Hunt Griffith
* Scott Perlik
* Sarah White
* Nick Mehrle &#931;
* Manwei Chan &#931;
* George Gelashvili
* Michael Kelly

### Juniors (Class of 2017)
* Caroyln Ortega
* Julia Chavarry
* Natasha Sidhu
* Tarini Konchady
* Lauren Aldoroty
* Andrzej Novak
* Anna Carter
* Michael O'Connor

### Sophomores (Class of 2018)
* Hayley Austin
* Duan Li
* James Damewood
* Olivia Gebhardt
* Gary Rhoades
* Stephane Teste
* Alec Farid
* Jared McInerney
* Austin Hopkins
* Lance Corbett

### Freshmen (Class of 2019)
* Sophia Porter
* Kyle Sullivan
* Theo Cooper
* Tina Nguyen
* Wenzer Qin
* Sumita Rajpuronit
* Kelsey Ishimoto
* Irina Plaks
* Anthony Flores
* Diva Parekh
* Austin Granmore
* Kevin Hsiau
* Mika Inadomi

To be on this list, you must have accumulated 15 "points" (that is, attended
three weekly SPS meetings). If you have met those criteria, but are not listed
here, please contact the Webmaster or Secretary.

# Alumni

### Class of 2015
* Georges Obied
* Quinn McFee
* Peranat Dayananda &#931;
* George Jiang

### Class of 2014:
* Ben Hartman &#931;
* Chris Mogni
* Liz Skerritt
* Jessica Noviello
* Evan Stafford &#931;
* Fletcher Boone
* Chris Mogni
* Dan Firestar
* Evan Rule
* Helen Fonda
* JohnPaul Kotyla
* Linnea Metcalf
* Marie Hepfer
* Matt Hill
* Matt Sheckells
* Oliver McNeely
* Valerie Chavez

### Class of 2013:

* Eddie Brooks
* Daniel Berman &#931;
* Paul O'Neil &#931;
* Jiyeong Kim
* Kevin Mather
* Burt Turner

## Class of 2012:

* Ben Cohen
* Katie Sparks

### Unsorted

* Ed Kardish
* Hang Yu
* Jeffrey Dandoy
* Jeremy McGale
* Jake Mokris (current grad student)
* Michael Lysak
* Miles McKey
* Michael Quintero
* Matthew Sartucci
* Mark Ziegler
* Tiara Swinson

### Members from 2004-2005:
[Old Member List](http://www.pha.jhu.edu/groups/sps/historical/Old%20--%202004-2005/people.htm):
will be added to main site soon.

The list of former members will be expanded soon.
