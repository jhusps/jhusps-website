Title: Groups

SPS collaborates with a handful of other groups on the Hopkins campus for
various activities and purposes.

(This page is still under construction)

## Association for Computing Machinery (ACM)

Frequently described (by me, anyway) as "the Computer Science version of SPS",
the ACM is an international computing organization. The Johns Hopkins student
chapter hosts weekly meetings with a speaker on computing topics and provides
a variety of other services.

Why do we collaborate with them? They host some of our infrastructure, among
other things. :) However, if you're into computational physics person, you
may be interested in their meetings.

Contact information for the ACM coming soon.

## Hopkinauts

Hopkinauts is a group of interested engineering and physics students who do
rocketry. They actually create payloads and launch rockets and recover data;
if you have any interest in rocketry or space science, you may be interested
in joining Hopkinauts.

If you *are* interested in joining Hopkinauts, email <hopkinauts@jhu.edu>

## PAGS: Physics and Astronomy Graduate Students

PAGS is our counterpart organization among the grad students; where we speak
for the undergrads in the physics department, they represent the graduate
students.

See [their website](http://sites.krieger.jhu.edu/pags/) for more.

## The HOP

We occasionally partner with the [Hopkins Organization for Programming](http://web.jhu.edu/hop)
in co-hosting movie-nights (Friday Night Films). For example, we hosted a
viewing of Gravity with them in the spring of 2014.

See their website for a full calendar of events at JHU, related to us or otherwise.

## JOSH

JOSH is Hopkins' all-female bollywood fusion dance group. We once co-hosted a
formal stargazing dance with them. [Their Facebook Page](https://www.facebook.com/JhuJOSH).
