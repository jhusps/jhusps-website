Title: Events

SPS meets every Friday at 4:00 PM in the Bloomberg Center for Physics and
Astronomy, room 462. Go 
[here]({filename}/graphics/campus_map.jpg) for a
campus map (Bloomberg is number 41).

We usually have free pizza and soda nearly every week, so come by!

We try to have a speaker come in every other week to talk about an exciting
topic, usually about his or her current research. On days without a speaker,
we generally have some kind of fun activity planned instead: in the past this
has been a movie but these days we do all kinds of stuff.

Although most speakers will be discussing physics related topics, people of
all majors are welcome at our meetings! You do not need to sign up or already
be a member to attend, all meetings are open to anyone interested. Although
our board is familiar with many of the people in the Physics Department, we
are always looking for new and exciting physics related topics outside of the
Physics Department. If you have any suggestions, please feel free to talk to
any of the board members.

# Other Events

In addition to our meetings and events, there are plenty of interesting talks
that happen in the physics department. We intend to provide a simple, unified
calendar / schedule of events for the following groups:

* Department Colloquia
* Department Seminars 

Want us to advertise your talk or event? Get
[in touch]({filename}/pages/contact.md)!

# Calendar

<iframe src="https://www.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=vqa80jcb2gbl4qqnmlpg5m7rp8%40group.calendar.google.com&amp;color=%23711616&amp;src=uf51rnmk3dpiqp87kafmrctlco%40group.calendar.google.com&amp;color=%235F6B02&amp;src=2ei7ta4bt38fi5cvfnsev6qisg%40group.calendar.google.com&amp;color=%23125A12&amp;src=bsqsldim7iuj63mt3sah2g3pbk%40group.calendar.google.com&amp;color=%2323164E&amp;src=phacalendars%40gmail.com&amp;color=%235229A3&amp;src=e35f1ckrmn82asvgkhiev488l4%40group.calendar.google.com&amp;color=%232952A3&amp;src=talks.stsci%40gmail.com&amp;color=%235F6B02&amp;ctz=America%2FNew_York" style=" border-width:0 " width="800" height="600" frameborder="0" scrolling="no"></iframe>
