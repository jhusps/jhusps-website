Title: Trips

Each spring SPS tries to organizes a physics related trip to somewhere in the country (and more recently the world).  The trip is open any member of SPS but preference is given to Senior members.  There is frequently a limit on the number of people who can attend; priority is given to more active members.

## 2015

SPS visited the [Brookhaven National Laboratory](http://www.bnl.gov/world/)
 in Upton, New York.

Photos are available [here](https://www.acm.jhu.edu/~sps/PhotoFloat/web/#!/brookhaven/).

## 2014
SPS visited the
[SLAC National Accelerator Laboratory](http://www6.slac.stanford.edu/) in 
Stanford, California. We also visited the Lawrence Berkeley National Laboratory
and met up with the [Berkeley SPS chapter](http://sps.berkeley.edu/).

Photos are available [here](https://www.acm.jhu.edu/~sps/PhotoFloat/web/#!/slac).

## 2012
SPS visited the Kennedy Space Center in Florida. Our photos are 
[here](http://www.pha.jhu.edu/groups/sps/photos/2012/index.html).

## 2009
SPS toured the [National Institute of Standards](http://www.nist.gov/). We also
had the rare opportunity to tour CERN.  More information about CERN and its
mission can be found [here](http://public.web.cern.ch/public/).  CERN is the 
home of the Large Hadron Collider. 

## 2008
SPS toured the 
[Goddard Space Flight Center](http://www.nasa.gov/centers/goddard/home/index.html)
in Greenbelt, MD.

## 2005
SPS went to Houston. Pictures from the Houston trip can be found
[here](http://www.pha.jhu.edu/~bschuyl1/SPS_trip_2005).

## 2002

For the 2002 trip, we went to Florida to view the Columbia Space Shuttle launch.
Part of the payload was the [Advanced Camera for Surveys](http://acs.pha.jhu.edu/),
an instrument developed at JHU.

<img src="http://www.pha.jhu.edu/groups/sps/historical/Old%20--%202003-2004/trip_pics/group.jpg" style="width: 500px;"/>

## 2001

The 2001 trip was to the National Radio Observatory in Greenbank, WV.

![A picture of the 2001 group.](http://www.pha.jhu.edu/groups/sps/historical/Old%20--%202003-2004/trip_pics/group2001.jpg)

From left to right: 
Justin Comparetta, 
Valerie Mikles, 
Sinan Arkin,
Brian Nord,
Phil Aminak,
Brian Smigielski,
Brad Frey, 
Emily Spahn,
Lucianne Walkowicz 

