#!/usr/bin/env python

"""
A helper script to pull out all the current members based on the attendance
roster. Currently runs on attendance.xlsm in same directory.
"""

import argparse
import datetime
import operator
import os

# This is a dependency.
import xlrd

# The name of the file.
workbook = "attendance.xlsm"

# We shouldn't have to hardcode this.
nameColumn = 1
yearColumn = 2
first = 2

# The names of classes
classes = ["Seniors", "Juniors", "Sophomores", "Freshmen"]

def getMemberNames(sheet, column):
	global first
	i = first
	names = []
	name = "Temp"
	# It's better to ask forgiveness than permission
	while True:
		try:
			name = sheet.cell(i, column).value
			names.append(name)
			i += 1
		except:
			break
	return names

def makeDictionary(names, dates):
	dictionary = {}
	for i in range(len(names)):
		name = names[i]
		date = dates[i]
		# Sorry, but if we don't have a year on file for you...
		if date != "":
			dictionary[name] = date

	return dictionary

def printMembers(dictionary, label, year):
	print "### " + label + " (Class of " + str(year) + ")"
	for name, value in dictionary.iteritems():
		if value == year:
			print "* " + name
	print ""

def main():
	global workbook, nameColumn, yearColumn, classes
	parser = argparse.ArgumentParser(description="Script to make SPS members list.")
	parser.add_argument("file", metavar="FILE", type=str, default=workbook, help="The Excel file.")
	args = parser.parse_args()

	# Open the workbook using this xlrd thing.
	path = os.path.expanduser(args.file)
	if not os.path.exists(path):
		print "Error: No such attendance file."
		return
	workbook = xlrd.open_workbook(path)
	workbook.sheet_names()
	sheet = workbook.sheet_by_index(0)

	# Get an array of the names and years
	names = getMemberNames(sheet, nameColumn)
	dates = getMemberNames(sheet, yearColumn)

	# Read them into a dictionary for easier usage
	dictionary = makeDictionary(names, dates)

	year = datetime.date.today().year
	current = year
	for i in range(4):
		current = year + i
		printMembers(dictionary, classes[i], current)


if __name__ == '__main__':
	main()
