# SPS Website v0.4

By the time I graduate I'll have nice documentation here, I swear.

Four previous versions of the site exist in [the historical folder](http://www.pha.jhu.edu/groups/sps/historical/),
hence the version number.

First, the [Pelican docs](http://docs.getpelican.com/en/3.3.0/) are your friend.
Markdown documentation is also your friend; please don't write content in HTML
or CSS. Markdown is so much easier!

Second, this git repository is attached to a buildbot instance currently running
on brosser.net but soon to be moved to an ACM virtual machine. This means that
any changes you make to the "master" branch will automatically be compiled and
pushed to the website.

Not familiar with git? Eventually I'll have links to easy documentation about it,
but if on Windows, [Github for Windows](https://windows.github.com/) is your
friend.

## Editing the site - easy mode

Because of buildbot, if all you want to do is replace some outdated text with less
outdated text, or fix a typo, or update the members list, you don't even have to 
bug me to do it.

You can edit all the content of the site *through bitbucket*, there is a nice
graphical in-the-web editor that will let you make changes and then commit your
changes.

Anyone with write access to the repository can do that (so, the current board).

Don't abuse this power, please. :)

## Posting new announcements

You *can* even create new files through the web interface, but I'd prefer that
you didn't, unless you're sure you know what you're doing.

Instead, if you want to post new announcements, I developed a GUI tool for
Windows (and Mac/Linux, if mono is installed) to do that, [jhusps-announce-gui](https://bitbucket.org/jhusps/jhusps-announce-gui)

It will ask you the right questions and then automatically make an announcement
and add it to bitbucket.

The tool is a work in progress, but it is good enough.

## Adding stuff using git

I'm putting announcements in content/announces/ for now.
The static pages are defined in content/pages/

More broadly, *all the content* is in the content/ folder somewhere.

To add anything to the website, all you need to do is:

```
git clone git@bitbucket.org:jhusps/jhusps.git
```

Then edit the files you want to edit, or add new markdown files in content/
or whatever. Then run:

```
git add .
git commit -m "Added [whatever you added]"
git push origin master
```

Thanks to the magic of [buildbot](http://buildbot.net/), this is all you need
to do! One of my server-build-slaves will automatically detect that you've
updated the source code and build and push the updated website to
oxygen.pha.jhu.edu.

If you *don't* want your changes to get built and pushed, commit to develop
instead of master (or another branch or fork).

## Physics account

Hopefully, you won't be building manually. However, should you have to...

To build and deploy, you need to have a @pha.jhu.edu account. This includes
having a @pha.jhu.edu email and also being able to login to oxygen, nitrogen,
and one other server that I can't remember.

Email <help@pha.jhu.edu> to get an account.

The automated system builds using my account, bjr. It should probably be
ported to use {account of current sysadmin}, but I don't really mind
continuing to use mine. Your choice, future administrator.

Note that this setup only works because my account has an SSH key
attached to it, though.

## Physics servers

The site can be accessed from oxygen.pha.jhu.edu and nitrogen.pha.jhu.edu.

It exists in ```/home/www/htdocs/groups/sps/```.

This folder is automounted; you have to explicitly cd into it to see it.

Hopefully you won't ever have to do this; just build and push the site.
(Or let buildbot build and push it for you).

## How to test:

Run the following command:

```
make html serve
```

This will compile the site and then run a test webserver on
http://localhost/8000

Go there to make sure the site is okay.

## How to build and deploy

Again, this probably won't ever be necessary, but: 

Run the following command, where [username] is your username on the physics
systems.

```
make SSH_USER=[username] html ssh_upload
```

It will ask you for your password.

Do *not* use ```make rsync_upload```, whatever you do, it will try to delete
everything in the SPS website folder on the server, including historical/.

## Credits:

* Ben Rosser <bjr@pha.jhu.edu>
* SPS webmasters and admins past