# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Society of Physics Students'
SITENAME = u'Society of Physics Students'
SITEURL = ''

TIMEZONE = 'America/New_York'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

# Links!
LINKS =  (('Physics Department', 'http://physics-astronomy.jhu.edu/'),
          ('PUC Lab', 'http://web1.johnshopkins.edu/puc/'),
          ('SPS Mailing List', 'http://mail.pha.jhu.edu/mailman/listinfo/sps'),
          ('Physics Course Catalog', 'http://e-catalog.jhu.edu/departments-program-requirements-and-courses/arts-sciences/physics-astronomy/#courseinventory'),
          ('SPS National', 'http://www.spsnational.org/'),
          ('NSF Internships', 'https://www.nsf.gov/crssprgm/reu/reu_search.jsp'))

# Social widget; need a better name...
SOCIAL = (('Weekly Meetings At:\n 4 PM on Fridays in Bloomberg 462', 'http://pha.jhu.edu/groups/sps/pages/events.html'),
          ('Contact the President, Joseph Cleary', 'mailto:jcleary3@jhu.edu'),
          ('Contact the Webmaster (or File a Bug)', 'https://bitbucket.org/jhusps/jhusps-website/issues/new'),
          ('Our Photos', 'https://www.acm.jhu.edu/~sps/PhotoFloat/web/'),
          ('Our IRC Channel', 'http://pha.jhu.edu/groups/sps/html/chat.html'),
          ('Our Mailing List', 'mailto:sps@pha.jhu.edu'))

# The number of articles displayed on the page.
# I don't know what a reasonable default is, let's say 10 for now?
DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

STATIC_EXCLUDE_SOURCES = False
STATIC_PATHS = ['html', 'graphics', 'extras', 'images']

# The default category
DEFAULT_CATEGORY = 'announce'

# We want the summary to be the same as the article
# At least, I think we do
SUMMARY_MAX_LENGTH = None

# Don't clutter the toplevel directory
ARTICLE_URL = 'posts/{slug}.html'
ARTICLE_SAVE_AS = 'posts/{slug}.html'

# Add favicon:
#EXTRA_PATH_METADATA = {
#    'extras/favicon.ico': {'path': 'favicon.ico'},
#}

# Add theme
THEME = './themes/pelican-bootstrap3/'

# Theme configuration
DISPLAY_CATEGORIES_ON_SIDEBAR = True
FAVICON = 'extras/favicon.ico'

# Don't display tags on the sidebar.
DISPLAY_TAGS_ON_SIDEBAR = False

# Don't display categories on the top menu.
DISPLAY_CATEGORIES_ON_MENU = False

# Plugins
# This should NOT be necessary. I hate EVERYTHING.
import sys
sys.path.append("./plugins")
import render_math
PLUGINS = [render_math]
